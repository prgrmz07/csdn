> [GameMaker ](https://www.yoyogames.com/zh-CN/gamemaker) + 核心逻辑状态图


> 满足 ： 平衡性+各方从起节点到赢节点的等概率性     的 状态图，都是可用的游戏



## [GameMaker ](https://www.yoyogames.com/zh-CN/gamemaker) 类似物 [godot  ](https://github.com/godotengine/godot):

> [godot github](https://github.com/godotengine/godot)
> [godot download](https://godotengine.org/download/windows)
[godotengine](https://downloads.tuxfamily.org/godotengine/3.4.2/)
[godot 3.4.2 模板](https://downloads.tuxfamily.org/godotengine/3.4.2/Godot_v3.4.2-stable_export_templates.tpz)


##  生成办法
1.  对一个既存的正常游戏godot脚本  进行松弛化  得到另一个类似的游戏
2. 先圈定一个游戏空间,  在该空间中搜索合法的游戏
3. ...
4. 选择题:  全部单个客观:  剑砍物体、炸弹爆炸破坏周边、重力、破损建筑物砖块掉落砸死人物...，从全部单个客观中选择少量客观试着构建一个游戏g的客观列表pl
5. 针对g的客观列表pl   产生客观间的初始交互关系r ,  游戏g被完全明确的定义为 客观列表pl和客观间的交互关系r
6. 游戏g的评价函数(损失函数)v。   玩10次游戏g   然后 统计双方角色的获胜比与1的距离(平衡性)、各次获胜时双方剩余生命的比值与1的距离(刺激性)、...，这样不直接评价pl和r    而是评价pl和r  (即g)  产生的实例   更容易
7. 是否能找出pl、r  对评价v的贡献数学关系，如果能 则 可以利用梯度下降以更新pl、r  从而寻找到更优的的pl、r，这看起来像GAN还是像概率图模型?


## godot 举例

```bash
# Open JDK (version 8 is required, more recent versions won't work)

# Creating key for debug
keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999 -deststoretype pkcs12

# Creating key for release
keytool -v -genkey -keystore mygame.keystore -alias mygame -keyalg RSA -validity 10000


#keytool路径:  C:\app\Java\jdk8u282-b08\bin\keytool.exe
```



![在这里插入图片描述](https://img-blog.csdnimg.cn/41ecfe8af0844a4ab5ec0207e897f8fa.png)
