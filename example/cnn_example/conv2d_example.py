import torch

import torch.nn.functional


#########
from typing import List

from torch.nn.parameter import Parameter
from torch.optim.adadelta import Adadelta

_TRAIN_SAMPLE_CNT,PIC_H,PIC_W=None,None,None
_TEST_SAMPLE_CNT=None
PIC_HW=None

LINEAR_1_OUT_SIZE=64
R_LINEAR_1_OUT_SIZE=LINEAR_1_OUT_SIZE
CLASS_CNT=10

# N0=28
# H=N0; W=N0
# HW=N0*N0#28*28

#  (xTrain,yTrain,xTest,yTest) = loadMnist() # -> (numpy.ndarray,numpy.ndarray,numpy.ndarray,numpy.ndarray)

import numpy
def loadMnist() -> (numpy.ndarray,numpy.ndarray,numpy.ndarray,numpy.ndarray):
    """

    :return:  (xTrain,yTrain,xTest,yTest)
    """
    global _TRAIN_SAMPLE_CNT
    global PIC_H
    global PIC_W
    global _TEST_SAMPLE_CNT
    global PIC_HW

    from tensorflow import keras #修改点: tensorflow:2.6.2,keras:2.6.0 此版本下,  import keras 换成 from tensorflow import keras
    import tensorflow
    print(f"keras.__version__:{keras.__version__}")#2.6.0
    print(f"tensorflow.__version__:{tensorflow.__version__}")#2.6.2


    # avatar_img_path = "/kaggle/working/data"


    import os

    import cv2

    # img_dataset=load_data()
    # img_dataset.shape
    xTrain:numpy.ndarray; label_train:numpy.ndarray; xTest:numpy.ndarray; label_test:numpy.ndarray
    yTrain:numpy.ndarray; yTest:numpy.ndarray

    #%userprofile%\.keras\datasets\mnist.npz
    (xTrain, label_train), (xTest, label_test) = keras.datasets.mnist.load_data()

    # x_train.shape,y_train.shape, x_test.shape, label_test.shape
    # (60000, 28, 28), (60000,), (10000, 28, 28), (10000,)
    _TRAIN_SAMPLE_CNT,PIC_H,PIC_W=xTrain.shape
    PIC_HW=PIC_H*PIC_W
    xTrain=xTrain.reshape((-1, PIC_H * PIC_W))
    xTest=xTest.reshape((-1, PIC_H * PIC_W))

    _TEST_SAMPLE_CNT=label_test.shape[0]


    from sklearn import preprocessing


    #pytorch 的 y 不需要 oneHot
    #_label_train是1列多行的样子.  _label_train.shape : (60000, 1)
    yTrain=label_train
    # y_train.shape:(60000) ; y_train.dtype: dtype('int')

    CLASS_CNT=yTrain.shape[0]

    yTest=label_test
    # y_test.shape:(10000) ; y_test.dtype: dtype('int')

    xTrainMinMaxScaler:preprocessing.MinMaxScaler; xTestMinMaxScaler:preprocessing.MinMaxScaler
    xTrainMinMaxScaler=preprocessing.MinMaxScaler()
    xTestMinMaxScaler=preprocessing.MinMaxScaler()

    # x_train.dtype: dtype('uint8') -> dtype('float64')
    xTrain=xTrainMinMaxScaler.fit_transform(xTrain)

    # x_test.dtype: dtype('uint8') -> dtype('float64')
    xTest = xTestMinMaxScaler.fit_transform(xTest)

    xTrain=xTrain.reshape((-1,  PIC_H , PIC_W))
    xTest=xTest.reshape((-1,  PIC_H , PIC_W))

    return (xTrain,yTrain,xTest,yTest)

xTrain:numpy.ndarray;yTrain:numpy.ndarray;xTest:numpy.ndarray;yTest:numpy.ndarray
(xTrain,yTrain,xTest,yTest) = loadMnist()

import plotly.express
import plotly.graph_objects
import plotly.subplots

print(f"yTrain[0]:{yTrain[0]}")
print(f"yTrain[1]:{yTrain[1]}")
# fig=plotly.graph_objects.Figure()
fig=plotly.subplots.make_subplots(rows=1,cols=2,shared_xaxes=True,shared_yaxes=True)
fig.add_trace(trace=plotly.express.imshow(xTrain[0]).data[0],row=1,col=1)
fig.add_trace(trace=plotly.express.imshow(xTrain[1]).data[0],row=1,col=2)
fig.show()
