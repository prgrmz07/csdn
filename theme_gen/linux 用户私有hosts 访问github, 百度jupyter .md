> [aistudio](https://aistudio.baidu.com/ )， 新建项目，进入jupyter notebook环境
```bash
#手工下载 github host文件:  https://raw.githubusercontent.com/521xueweihan/GitHub520/main/hosts

#上传该hosts文件到jupyter notebook 

#重命名该文件 为当前linux用户的 私有hosts :  ~/.hosts
mv hosts ~/.hosts
#不要用全局hosts : /etc/hosts,  此文件无权限修改

#接着可以正常使用github了
! git clone https://github.com/xiaohuiduan/dcgan_anime_avatars

#等效于以下命令:
#curl https://raw.githubusercontent.com/521xueweihan/GitHub520/main/hosts -O ~/.hosts

```
