import keras
import tensorflow
print(keras.__version__)
print(tensorflow.__version__)

# avatar_img_path = "/kaggle/working/data"


import os

import cv2
import numpy
import numpy as np

# img_dataset=load_data()
# img_dataset.shape
xTrain:numpy.ndarray; label_train:numpy.ndarray; xTest:numpy.ndarray; label_test:numpy.ndarray
yTrain:numpy.ndarray; yTest:numpy.ndarray

#%userprofile%\.keras\datasets\mnist.npz
(xTrain, label_train), (xTest, label_test) = keras.datasets.mnist.load_data()

# x_train.shape,y_train.shape, x_test.shape, label_test.shape
# (60000, 28, 28), (60000,), (10000, 28, 28), (10000,)
_TRAIN_SAMPLE_CNT,PIC_H,PIC_W=xTrain.shape
xTrain=xTrain.reshape((-1, PIC_H * PIC_W))
xTest=xTest.reshape((-1, PIC_H * PIC_W))

_TEST_SAMPLE_CNT=label_test.shape[0]


from sklearn import preprocessing

labelOneHotEncoder:preprocessing.OneHotEncoder
labelOneHotEncoder=preprocessing.OneHotEncoder(sparse=False)

#_label_train是1列多行的样子.  _label_train.shape : (60000, 1)
_labelTrain=label_train.reshape(_TRAIN_SAMPLE_CNT, 1)
yTrain=labelOneHotEncoder.fit_transform(_labelTrain)
# y_train.shape:(60000, 10) ; y_train.dtype: dtype('float64')
# numpy.testing.assert_array_equal(labelOneHotEncoder.inverse_transform(y_train).reshape( (_TRAIN_SAMPLE_CNT) ),label_train)

_,CLASS_CNT=yTrain.shape

_labelTest=label_test.reshape(_TEST_SAMPLE_CNT, 1)
yTest=labelOneHotEncoder.fit_transform(_labelTest)
# y_test.shape:(10000, 10) ; y_test.dtype: dtype('float64')
# numpy.testing.assert_array_equal(labelOneHotEncoder.inverse_transform(y_test).reshape( (_TEST_SAMPLE_CNT) ) , label_test)

xTrainMinMaxScaler:preprocessing.MinMaxScaler; xTestMinMaxScaler:preprocessing.MinMaxScaler
xTrainMinMaxScaler=preprocessing.MinMaxScaler()
xTestMinMaxScaler=preprocessing.MinMaxScaler()

# x_train.dtype: dtype('uint8') -> dtype('float64')
xTrain=xTrainMinMaxScaler.fit_transform(xTrain)

# x_test.dtype: dtype('uint8') -> dtype('float64')
xTest = xTestMinMaxScaler.fit_transform(xTest)


# # noise的维度
# noise_dim = 100
# # 图片的shape
# image_shape = (64,64,3)

from keras.models import Sequential,Model
from keras.layers import Dropout, Conv2D, Dense,  LeakyReLU, Input,Reshape,  Flatten,  Conv2DTranspose
#修改点: tensorflow.
from tensorflow.keras.optimizers import Adam,RMSprop

# def build_G():
#     """
#     构建生成器
#     :return:
#     """
#     model = Sequential()
#     model.add(Input(shape=noise_dim))
#
#     model.add(Dense(128*32*32))
#     model.add(LeakyReLU(0.2))
#     model.add(Reshape((32,32,128)))
#
#     model.add(Conv2D(256,5,padding='same'))
#     model.add(LeakyReLU(0.2))
#
#     model.add(Conv2DTranspose(256,4,strides=2,padding='same'))
#     model.add(LeakyReLU(0.2))
#
#     model.add(Conv2D(256,5,padding='same'))
#     model.add(LeakyReLU(0.2))
#
#
#     model.add(Conv2D(256,5,padding='same'))
#     model.add(LeakyReLU(0.2))
#
#     model.add(Conv2D(3,7,activation='tanh',padding='same'))
#
#     return model

# G = build_G()

opt_d=Adam(learning_rate=0.0002, beta_1=0.5)
# opt_gan=Adam(learning_rate=0.0002, beta_1=0.5)


def buildModel():
    """
    构建判别器
    :return: 
    """
    model = Sequential()
    
    # # 卷积层
    # model.add(Conv2D(128,3,input_shape = image_shape))
    # model.add(LeakyReLU(0.2))

    model.add(Dense(64))

    # model.add(Conv2D(128,4, strides=2))
    # model.add(LeakyReLU(0.2))
    #
    # model.add(Conv2D(128,4, strides=2))
    # model.add(LeakyReLU(0.2))
    #
    # model.add(Flatten())
    # model.add(Dropout(0.4))

    model.add(Dense(CLASS_CNT,activation='softmax'))
    # keras.losses.Loss
    # keras.losses.CategoricalCrossentropy
    model.compile(loss='categorical_crossentropy', optimizer=opt_d
                  , metrics=["accuracy"]
                  )

    # model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    # model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

    return model
net = buildModel()

#
# def build_gan():
#     """
#     构建GAN网络
#     :return:
#     """
#     # 冷冻判别器，也就是在训练的时候只优化G的网络权重，而对D保持不变
#     net.trainable = False
#     # GAN网络的输入
#     gan_input = Input(shape=(noise_dim,))
#     # GAN网络的输出
#     gan_out = net(G(gan_input))
#     # 构建网络
#     gan = Model(gan_input,gan_out)
#     # 编译GAN网络，使用Adam优化器，以及加上交叉熵损失函数（一般用于二分类）
#     gan.compile(loss='binary_crossentropy',optimizer=opt_gan)
#     return gan
# GAN = build_gan()







import datetime
def train(epochs=100, batch_size=64):
    """
    训练函数
    :param epochs: 训练的次数
    :param batch_size: 批尺寸
    :return:
    """
    # 判别器损失
    discriminator_loss = 0
    # 生成器损失
    generator_loss = 0
    
    # img_dataset.shape[0] / batch_size 代表这个数据可以分为几个批次进行训练
    batch_cnt = int(_TRAIN_SAMPLE_CNT / batch_size)
    
    for i in range(epochs):
        pre_now=datetime.datetime.now()
        for batch_index in range(batch_cnt):
            # 按批次加载数据
            beginIndex=batch_index * batch_size
            endIndex=(batch_index + 1) * batch_size
            x=xTrain[beginIndex: endIndex]
            y=yTrain[beginIndex:endIndex]
            # x = load_batch(xTrain, batch_size,batch_index )
            # 产生noise
            # noise = sample_noise(batch_size)
            # # G网络产生图片
            # generated_images = G.predict(noise)
            # # 产生为1的标签
            # y_real = np.ones(batch_size)
            # # 将1标签的范围变成[0.8 , 1.0]
            # y_real = smooth_pos_labels(y_real)
            # # 产生为0的标签
            # y_fake = np.zeros(batch_size)
            # # 将0标签的范围变成[0.0 , 0.3]
            # y_fake = smooth_neg_labels(y_fake)
            # # 训练真图片loss
            d_loss_real = net.train_on_batch(x, y)
            # # 训练假图片loss
            # d_loss_fake = net.train_on_batch(generated_images, y_fake)
            #
            # discriminator_loss = d_loss_real + d_loss_fake
            # # 产生为1的标签
            # y_real_g = np.ones(batch_size)
            # # 训练GAN网络，input = fake_img ,label = 1
            # generator_loss = GAN.train_on_batch(noise, y_real_g)
        
        now=datetime.datetime.now()
        lossTest=net.evaluate(xTest, yTest,verbose=0)
        print(f'[Epoch {i}].   loss : {d_loss_real}. lossTest:{lossTest} , time spent: {now-pre_now}')

        # 每个epoch保存一次。
        # if i%1 == 0:
        #     # 随机产生(25,100)的noise
        #     test_noise = sample_noise(25)
        #     # 使用G网络生成25张图偏
        #     test_images = G.predict(test_noise)
        #     # show 预测 img
        #     # show_images(test_images,i)
        #     GAN.save_weights(f'model_{i}.h5')

train(epochs=2, batch_size=256)

net.summary()