# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:09.609369Z","iopub.execute_input":"2022-02-18T07:01:09.609784Z","iopub.status.idle":"2022-02-18T07:01:09.737437Z","shell.execute_reply.started":"2022-02-18T07:01:09.609749Z","shell.execute_reply":"2022-02-18T07:01:09.736539Z"},"jupyter":{"outputs_hidden":false}}
%%bash
# git clone https://github.com/xiaohuiduan/dcgan_anime_avatars
# mv /kaggle/working/dcgan_anime_avatars/data /kaggle/working/
rm -fr /kaggle/working/model_*.h5
ls /kaggle/working/data/ | wc -l

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:09.739513Z","iopub.execute_input":"2022-02-18T07:01:09.739770Z","iopub.status.idle":"2022-02-18T07:01:09.746770Z","shell.execute_reply.started":"2022-02-18T07:01:09.739743Z","shell.execute_reply":"2022-02-18T07:01:09.745941Z"},"jupyter":{"outputs_hidden":false}}
import keras
import tensorflow
print(keras.__version__)
print(tensorflow.__version__)

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:09.748215Z","iopub.execute_input":"2022-02-18T07:01:09.748703Z","iopub.status.idle":"2022-02-18T07:01:14.792227Z","shell.execute_reply.started":"2022-02-18T07:01:09.748667Z","shell.execute_reply":"2022-02-18T07:01:14.791463Z"},"jupyter":{"outputs_hidden":false}}
# 数据集的位置
avatar_img_path = "/kaggle/working/data"

import os

import cv2
import numpy as np
def load_data():
    """
    加载数据集
    :return: 返回numpy数组
    """
    all_images = []
    # 从本地文件读取图片加载到images_data中。
    for image_name in os.listdir(avatar_img_path):
        try:
            image = cv2.cvtColor(
                cv2.resize(
                    cv2.imread(os.path.join(avatar_img_path,image_name), cv2.IMREAD_COLOR),
                    (64, 64)
                    ),cv2.COLOR_BGR2RGB
                )
            all_images.append(image)
        except:
            continue
        
    all_images = np.array(all_images)
    # 将图片数值变成[-1,1]
    all_images = (all_images - 127.5) / 127.5
    # 将数据随机排序
    np.random.shuffle(all_images)
    return all_images[:10000]#只取前5000张以节省训练时间

img_dataset=load_data()
img_dataset.shape

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:14.793633Z","iopub.execute_input":"2022-02-18T07:01:14.794210Z","iopub.status.idle":"2022-02-18T07:01:14.801252Z","shell.execute_reply.started":"2022-02-18T07:01:14.794173Z","shell.execute_reply":"2022-02-18T07:01:14.800302Z"},"jupyter":{"outputs_hidden":false}}

import matplotlib.pyplot as plt
def show_images(images,index = -1):
    """
    展示并保存图片
    :param images: 需要show的图片
    :param index: 图片名
    :return:
    """
    plt.figure()
    for i, image in enumerate(images):
        ax = plt.subplot(5, 5, i+1)
        plt.axis('off')
        img=image-image.min(); img=img/img.max() #[-1,+1]转到[0,1]
        # print(img.max(),img.min(),img.mean())
        plt.imshow(img)
    plt.savefig("data_%d.png"%index)
    plt.show()

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:14.803979Z","iopub.execute_input":"2022-02-18T07:01:14.804676Z","iopub.status.idle":"2022-02-18T07:01:15.621759Z","shell.execute_reply.started":"2022-02-18T07:01:14.804640Z","shell.execute_reply":"2022-02-18T07:01:15.621081Z"},"jupyter":{"outputs_hidden":false}}
show_images(img_dataset[0: 25])

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.623270Z","iopub.execute_input":"2022-02-18T07:01:15.623766Z","iopub.status.idle":"2022-02-18T07:01:15.627984Z","shell.execute_reply.started":"2022-02-18T07:01:15.623731Z","shell.execute_reply":"2022-02-18T07:01:15.627396Z"},"jupyter":{"outputs_hidden":false}}
# noise的维度
noise_dim = 100
# 图片的shape
image_shape = (64,64,3)

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.629331Z","iopub.execute_input":"2022-02-18T07:01:15.630703Z","iopub.status.idle":"2022-02-18T07:01:15.637288Z","shell.execute_reply.started":"2022-02-18T07:01:15.630667Z","shell.execute_reply":"2022-02-18T07:01:15.636517Z"},"jupyter":{"outputs_hidden":false}}
from keras.models import Sequential,Model
from keras.layers import Dropout, Conv2D, Dense,  LeakyReLU, Input,Reshape,  Flatten,  Conv2DTranspose
#修改点: tensorflow.
from tensorflow.keras.optimizers import Adam,RMSprop

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.638870Z","iopub.execute_input":"2022-02-18T07:01:15.639502Z","iopub.status.idle":"2022-02-18T07:01:15.721013Z","shell.execute_reply.started":"2022-02-18T07:01:15.639408Z","shell.execute_reply":"2022-02-18T07:01:15.720396Z"},"jupyter":{"outputs_hidden":false}}
def build_G():
    """
    构建生成器
    :return:
    """
    model = Sequential()
    model.add(Input(shape=noise_dim))

    model.add(Dense(128*32*32))
    model.add(LeakyReLU(0.2))
    model.add(Reshape((32,32,128)))
    
    model.add(Conv2D(256,5,padding='same'))
    model.add(LeakyReLU(0.2))
    
    model.add(Conv2DTranspose(256,4,strides=2,padding='same'))
    model.add(LeakyReLU(0.2))
    
    model.add(Conv2D(256,5,padding='same'))
    model.add(LeakyReLU(0.2))
    
    
    model.add(Conv2D(256,5,padding='same'))
    model.add(LeakyReLU(0.2))
    
    model.add(Conv2D(3,7,activation='tanh',padding='same'))
    
    return model

G = build_G()

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.722285Z","iopub.execute_input":"2022-02-18T07:01:15.722528Z","iopub.status.idle":"2022-02-18T07:01:15.784306Z","shell.execute_reply.started":"2022-02-18T07:01:15.722494Z","shell.execute_reply":"2022-02-18T07:01:15.783698Z"},"jupyter":{"outputs_hidden":false}}
opt_d=Adam(learning_rate=0.0002, beta_1=0.5)
opt_gan=Adam(learning_rate=0.0002, beta_1=0.5)

# opt_d=RMSprop(lr=0.0004, clipvalue=1.0, decay=1e-8)
# opt_gan=RMSprop(lr=0.0004, clipvalue=1.0, decay=1e-8)
#RMSprop(lr=0.0004, clipvalue=1.0, decay=1e-8)
#Adam(learning_rate=0.0002, beta_1=0.5)
    
def build_D():
    """
    构建判别器
    :return: 
    """
    model = Sequential()
    
    # 卷积层
    model.add(Conv2D(128,3,input_shape = image_shape))
    model.add(LeakyReLU(0.2))

    model.add(Conv2D(128,4, strides=2))
    model.add(LeakyReLU(0.2))

    model.add(Conv2D(128,4, strides=2))
    model.add(LeakyReLU(0.2))

    model.add(Conv2D(128,4, strides=2))
    model.add(LeakyReLU(0.2))
    
    model.add(Flatten())
    model.add(Dropout(0.4))

    model.add(Dense(1,activation='sigmoid'))
    
    model.compile(loss='binary_crossentropy',
              optimizer=opt_d) 
    return model
D = build_D()

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.785480Z","iopub.execute_input":"2022-02-18T07:01:15.785737Z","iopub.status.idle":"2022-02-18T07:01:15.847913Z","shell.execute_reply.started":"2022-02-18T07:01:15.785704Z","shell.execute_reply":"2022-02-18T07:01:15.847301Z"},"jupyter":{"outputs_hidden":false}}

def build_gan():
    """
    构建GAN网络
    :return:
    """
    # 冷冻判别器，也就是在训练的时候只优化G的网络权重，而对D保持不变
    D.trainable = False
    # GAN网络的输入
    gan_input = Input(shape=(noise_dim,))
    # GAN网络的输出
    gan_out = D(G(gan_input))
    # 构建网络
    gan = Model(gan_input,gan_out)
    # 编译GAN网络，使用Adam优化器，以及加上交叉熵损失函数（一般用于二分类）
    gan.compile(loss='binary_crossentropy',optimizer=opt_gan)
    return gan
GAN = build_gan()

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.848921Z","iopub.execute_input":"2022-02-18T07:01:15.849171Z","iopub.status.idle":"2022-02-18T07:01:15.855415Z","shell.execute_reply.started":"2022-02-18T07:01:15.849139Z","shell.execute_reply":"2022-02-18T07:01:15.854766Z"},"jupyter":{"outputs_hidden":false}}

def sample_noise(batch_size):
    """
    随机产生正态分布（0，1）的noise
    :param batch_size:
    :return: 返回的shape为(batch_size,noise)
    """
    return np.random.normal(size=(batch_size, noise_dim))

def smooth_pos_labels(y):
    """
    使得true label的值的范围为[0.8,1]
    :param y:
    :return:
    """
    return y - (np.random.random(y.shape) * 0.2)

def smooth_neg_labels(y):
    """
    使得fake label的值的范围为[0.0,0.3]
    :param y:
    :return:
    """
    return y + np.random.random(y.shape) * 0.3

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.856729Z","iopub.execute_input":"2022-02-18T07:01:15.857167Z","iopub.status.idle":"2022-02-18T07:01:15.863617Z","shell.execute_reply.started":"2022-02-18T07:01:15.857103Z","shell.execute_reply":"2022-02-18T07:01:15.862696Z"},"jupyter":{"outputs_hidden":false}}

def load_batch(data, batch_size,index):
    """
    按批次加载图片
    :param data: 图片数据集
    :param batch_size: 批次大小
    :param index: 批次序号
    :return:
    """
    return data[index*batch_size: (index+1)*batch_size]

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.865127Z","iopub.execute_input":"2022-02-18T07:01:15.865766Z","iopub.status.idle":"2022-02-18T07:01:15.879254Z","shell.execute_reply.started":"2022-02-18T07:01:15.865727Z","shell.execute_reply":"2022-02-18T07:01:15.878611Z"},"jupyter":{"outputs_hidden":false}}
import datetime
def train(epochs=100, batch_size=64):
    """
    训练函数
    :param epochs: 训练的次数
    :param batch_size: 批尺寸
    :return:
    """
    # 判别器损失
    discriminator_loss = 0
    # 生成器损失
    generator_loss = 0
    
    # img_dataset.shape[0] / batch_size 代表这个数据可以分为几个批次进行训练
    n_batches = int(img_dataset.shape[0] / batch_size)
    
    for i in range(epochs):
        pre_now=datetime.datetime.now()
        for batch_index in range(n_batches):
            # 按批次加载数据
            x = load_batch(img_dataset, batch_size,batch_index )
            # 产生noise
            noise = sample_noise(batch_size)
            # G网络产生图片
            generated_images = G.predict(noise)
            # 产生为1的标签
            y_real = np.ones(batch_size)
            # 将1标签的范围变成[0.8 , 1.0]
            y_real = smooth_pos_labels(y_real)
            # 产生为0的标签
            y_fake = np.zeros(batch_size)
            # 将0标签的范围变成[0.0 , 0.3]
            y_fake = smooth_neg_labels(y_fake)
            # 训练真图片loss
            d_loss_real = D.train_on_batch(x, y_real)
            # 训练假图片loss
            d_loss_fake = D.train_on_batch(generated_images, y_fake)

            discriminator_loss = d_loss_real + d_loss_fake
            # 产生为1的标签
            y_real_g = np.ones(batch_size)
            # 训练GAN网络，input = fake_img ,label = 1
            generator_loss = GAN.train_on_batch(noise, y_real_g)
        
        now=datetime.datetime.now()
        print(f'[Epoch {i}]. Discriminator loss : {discriminator_loss}. Generator_loss: {generator_loss}, time spent: {now-pre_now}')

        # 每个epoch保存一次。
        if i%1 == 0:
            # 随机产生(25,100)的noise
            test_noise = sample_noise(25)
            # 使用G网络生成25张图偏
            test_images = G.predict(test_noise)
            # show 预测 img
            show_images(test_images,i)
            GAN.save_weights(f'model_{i}.h5')

# %% [code] {"execution":{"iopub.status.busy":"2022-02-18T07:01:15.882122Z","iopub.execute_input":"2022-02-18T07:01:15.882618Z","iopub.status.idle":"2022-02-18T09:56:11.226531Z","shell.execute_reply.started":"2022-02-18T07:01:15.882568Z","shell.execute_reply":"2022-02-18T09:56:11.225746Z"},"jupyter":{"outputs_hidden":false}}
train(epochs=100, batch_size=256)

# %% [code] {"jupyter":{"outputs_hidden":false}}
