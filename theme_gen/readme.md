# theme generate 

## study

> [李宏毅机器学习笔记(LeeML-Notes)](https://github.com/datawhalechina/leeml-notes)




> [《动手学深度学习》](https://zh.d2l.ai/)


## theme generate related
> [Iconify 2004.03179](https://arxiv.org/pdf/2004.03179.pdf)

> [IconTransfer 图标生成技术预研](https://benzblog.site/2019-07-04-IconTransfer%20%E5%9B%BE%E6%A0%87%E7%94%9F%E6%88%90%E6%8A%80%E6%9C%AF%E9%A2%84%E7%A0%94/)


## GAN

> [GAN论文汇总，包含code,zhangqianhui/AdversarialNetsPapers ](https://github.com/zhangqianhui/AdversarialNetsPapers)

> [李宏毅深度学习笔记—GAN (第三方整理 感觉不全)](https://zhuanlan.zhihu.com/p/84783062)

> [通俗理解生成对抗网络GAN (知乎某某...)](https://zhuanlan.zhihu.com/p/33752313)

> [通俗理解生成对抗网络GAN](https://zhuanlan.zhihu.com/p/33752313)

> [干货 _ 深入浅出 GAN·原理篇文字版（完整）.html](https://mp.weixin.qq.com/s/dVDDMXS6RA_NWc4EpLQJdw)   ,,, [备用链接](https://gitlab.com/prgrmz07/csdn/-/blob/main/theme_gen/%E5%B9%B2%E8%B4%A7___%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA_GAN_%E5%8E%9F%E7%90%86%E7%AF%87%E6%96%87%E5%AD%97%E7%89%88_%E5%AE%8C%E6%95%B4_.html)

> [GAN学习指南：从原理入门到制作生成Demo](https://zhuanlan.zhihu.com/p/24767059)

> [GAN实例:  DCGAN动漫头像生成, 有代码](https://www.cnblogs.com/xiaohuiduan/p/13274675.html)

> [GAN实例:  DCGAN动漫头像生成, 有代码, 有数据集](https://github.com/xiaohuiduan/dcgan_anime_avatars)

> [GAN实例:  DCGAN动漫头像 代码原始git仓库? DCGAN-Anime](https://github.com/haryoa/DCGAN-Anime)

> [GAN实例:  DCGAN动漫头像 原博文?](https://towardsdatascience.com/generate-anime-style-face-using-dcgan-and-explore-its-latent-feature-representation-ae0e905f3974)

> [GAN实例: DCGAN动漫头像 jupyter notebook, 无数据集](https://colab.research.google.com/github/haryoa/DCGAN-Anime/blob/master/Experiment_Anime_GAN.ipynb),,,,,,,  [备用链接](https://github.com/haryoa/DCGAN-Anime/blob/master/Experiment_Anime_GAN.ipynb)

### GAN理解
> GAN 李宏毅 笔记 重要图: ![GAN 李宏毅 笔记 重要图](https://img-blog.csdnimg.cn/7bab15be6f674c258073559ff81209f7.png)
> 1. 固定G, 此时网络结构只有D, 只训练D
> 2. 固定D，此时网络结构为G->D, 其中D相当于损失函数(D的输出相当于损失函数, D的输出是优化目标), 只训练G


## 不好的GAN代码举例
> ![不好的GAN代码举例](https://img-blog.csdnimg.cn/24d1ba68dab74f798908a541f0b4c65e.png)




##  ...??1

>batch 间的顺序
>>batch1  、 batch2  都对 weight 做出了改变，现在问 ： 如果改变batch1 batch2 的顺序 所得 weight最终值应该是有丝毫差异的，这说明了什么？

>>batch2为什么要对weight做出这样的改变?    如果能区分出batch对weight的变更中的 有效成分 、无效成分，则 每次都可以丢弃 无效成分。 则 每次对weight的变更都是有效的 （每次学习都是有效的）

>>  将 batch顺序 及 各batch对weight作的改变 记录下  ，试着 从这些记录中 找出 些什么。


### 逆 weight 矩阵
1. 基础代码
> [pytorch 自制batch 的 mnist 正常运行](https://gitlab.com/prgrmz07/csdn/-/blob/04e7c11e1547f1c0ba87bc7139eceb60c5368c18/theme_gen/reverse_weight_pytorch_mnist.py)
2. 完工代码
> [pytorch mnist 正向网络Net 逆向网络RNet 正常运行，仅仅一层](https://gitlab.com/prgrmz07/csdn/-/blob/1fe5568fd54e9c223cd32d8feddd3014867fde3a/theme_gen/reverse_weight_pytorch_mnist.py)

> 比对 以上两次提交 ,  可以知道  逆向网络 是 怎样实现的 


> [网络结构文档](https://gitlab.com/prgrmz07/csdn/-/blob/1fe5568fd54e9c223cd32d8feddd3014867fde3a/theme_gen/reverse_weight_pytorch_mnist.drawio.pdf)


> [  逆weight矩阵 完工代码 kaggle运行](https://gitlab.com/prgrmz07/csdn/-/blob/8050d1b9a269718835d30d9957210e2c7fdd23a6/theme_gen/reverse-weight-pytorch-mnist.ipynb)
