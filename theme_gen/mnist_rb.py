from typing import List

from torch.nn.parameter import Parameter
from torch.optim.adadelta import Adadelta

_TRAIN_SAMPLE_CNT,PIC_H,PIC_W=None,None,None
_TEST_SAMPLE_CNT=None
PIC_HW=None

LINEAR_1_OUT_SIZE=64
R_LINEAR_1_OUT_SIZE=LINEAR_1_OUT_SIZE
CLASS_CNT=10

# %% [markdown]
#  (xTrain,yTrain,xTest,yTest) = loadMnist() # -> (numpy.ndarray,numpy.ndarray,numpy.ndarray,numpy.ndarray)

import numpy
import numpy as np
def loadMnist() -> (numpy.ndarray,numpy.ndarray,numpy.ndarray,numpy.ndarray):
    """

    :return:  (xTrain,yTrain,xTest,yTest)
    """
    global _TRAIN_SAMPLE_CNT
    global PIC_H
    global PIC_W
    global _TEST_SAMPLE_CNT
    global PIC_HW

    from tensorflow import keras #修改点: tensorflow:2.6.2,keras:2.6.0 此版本下,  import keras 换成 from tensorflow import keras
    import tensorflow
    print(keras.__version__)#2.6.0
    print(tensorflow.__version__)#2.6.2

    # avatar_img_path = "/kaggle/working/data"


    import os

    import cv2

    # img_dataset=load_data()
    # img_dataset.shape
    xTrain:numpy.ndarray; label_train:numpy.ndarray; xTest:numpy.ndarray; label_test:numpy.ndarray
    yTrain:numpy.ndarray; yTest:numpy.ndarray

    #%userprofile%\.keras\datasets\mnist.npz
    (xTrain, label_train), (xTest, label_test) = keras.datasets.mnist.load_data()

    # x_train.shape,y_train.shape, x_test.shape, label_test.shape
    # (60000, 28, 28), (60000,), (10000, 28, 28), (10000,)
    _TRAIN_SAMPLE_CNT,PIC_H,PIC_W=xTrain.shape
    PIC_HW=PIC_H*PIC_W
    xTrain=xTrain.reshape((-1, PIC_H * PIC_W))
    xTest=xTest.reshape((-1, PIC_H * PIC_W))

    _TEST_SAMPLE_CNT=label_test.shape[0]


    from sklearn import preprocessing


    #pytorch 的 y 不需要 oneHot
    #_label_train是1列多行的样子.  _label_train.shape : (60000, 1)
    yTrain=label_train
    # y_train.shape:(60000) ; y_train.dtype: dtype('int')

    CLASS_CNT=yTrain.shape[0]

    yTest=label_test
    # y_test.shape:(10000) ; y_test.dtype: dtype('int')

    xTrainMinMaxScaler:preprocessing.MinMaxScaler; xTestMinMaxScaler:preprocessing.MinMaxScaler
    xTrainMinMaxScaler=preprocessing.MinMaxScaler()
    xTestMinMaxScaler=preprocessing.MinMaxScaler()

    # x_train.dtype: dtype('uint8') -> dtype('float64')
    xTrain=xTrainMinMaxScaler.fit_transform(xTrain)

    # x_test.dtype: dtype('uint8') -> dtype('float64')
    xTest = xTestMinMaxScaler.fit_transform(xTest)

    xTrain=xTrain.reshape((-1, PIC_H , PIC_W))
    xTest=xTest.reshape((-1, PIC_H , PIC_W))
    return (xTrain,yTrain,xTest,yTest)

import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

class Args:
    def __init__(self):
        self.epochs = 4
        self.lr = 1.0
        self.gamma = 0.7
        self.seed = 1
        self.log_interval = 100
        self.save_model = True

    def update(self, d):
        self.__dict__.update(d.__dict__)


class TrainArgs(Args):
    def __init__(self):
        Args.__init__(self)
        self.batch_size = 50


class TestArgs(Args):
    def __init__(self):
        Args.__init__(self)
        self.batch_size = 1000


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.front_fc1 = nn.Linear(28*28, 64)
        self.front_fc2 = nn.Linear(64, 10)

    def forward(self, x):
#         print(f"xxx:{x.shape}")
        xflat=x.view(-1, 28*28)
        z_fc1 = self.front_fc1(xflat)
        o_fc1 = F.relu(z_fc1)
        o_fc2 = self.front_fc2(o_fc1)
        ŷ = F.log_softmax(o_fc2, dim=1)
        return ŷ

zeroPad2d:nn.ZeroPad2d=nn.ZeroPad2d((1, 1, 0, 0))

import plotly.express
import plotly.graph_objects
from plotly.subplots import make_subplots

def train(trainArgs:TrainArgs, model:Net,     xTrain:torch.Tensor,yTrain:torch.Tensor, optimizer:Adadelta,  epoch):
    model.train()#模型设置为训练模式
    batch_size=trainArgs.batch_size
    batch_cnt = int(_TRAIN_SAMPLE_CNT / batch_size)

    for batch_index in range(batch_cnt):
        beginIndex=batch_index * batch_size
        endIndex=(batch_index + 1) * batch_size
        x=xTrain[beginIndex: endIndex]
        y=yTrain[beginIndex:endIndex]
#         print(f"x:{x.shape},y:{y.shape}")
        plotly.express.imshow(x[0]).show()
        x=torch.squeeze(zeroPad2d( torch.unsqueeze(x,0)  ))

        # fig:plotly.graph_objects.Figure=plotly.graph_objects.Figure()
        fig = make_subplots( rows=14, cols=10,vertical_spacing=0,horizontal_spacing=0)
        # x_=x.reshape((-1, 14, 10, 2, 3))
        x_ = x.reshape((-1, 14, 2, 10, 3))
        for i in range(x_[0].shape[0]):#14
            for j in range(x_[0].shape[2]):#10
                # fig.add_trace(plotly.graph_objects.Image(x_[0][i,j]), i+1, j+1)
                fig.add_trace(plotly.express.imshow(x_[0][i,:, j].reshape((2,3))).data[0], i + 1, j + 1)
        fig.show()
        #在这里加断点，展示貌似是正确的

        # plotly.express.imshow(x.reshape((-1, 14, 10, 2, 3))[0, 0, 0]).show()

        #x.shape:(-1,28,30)
        #28:2*14,  30:3*10


        # plotly.express.imshow(x[0]).show()
        optimizer.zero_grad() # 用 optimizer.zero_grad() 还是 model.zero_grad(), 试了一下 感觉一样的. 但不知道两者差异
        # model.zero_grad()
        ŷ = model(x)
        # print(f"ŷ:{ŷ.shape} ,{ŷ.dtype} ; y:{y.shape},{y.dtype}")
        # output:torch.Size([50, 10]) ,torch.float32 ; y:torch.Size([50]),torch.int64
        loss = F.nll_loss(ŷ, y)
        loss.backward()#计算导数
        # optimizer.param_groups[0]["params"][0].grad[:, 100]#A.检查梯度
        # optimizer.step()#A. 用导数更新参数w #不更新w, 则再次计算导数 还是在原来的w点上计算的导数, 这是此处想要的效果.

        #尝试{
        if x[0][100] > 0.0 :
            paramList:List[Parameter]=optimizer.param_groups[0]["params"]
            # x[0][100] = 0.0
            optimizer.zero_grad()#清除优化器中记录的导数
            ŷ = model(x)#前向
            loss = F.nll_loss(ŷ, y)#损失
            loss.backward()#计算导数
            pass # paramList[0].grad[:, 100]#B.检查梯度. 此时paramList[0].grad[:, 100]是和A处的梯度一致
        #尝试}
        
        optimizer.step()#B. 用导数更新参数w #更新w, 则再次计算导数 便是在原来的w点附近的w点上计算的导数, 这是此处想要的效果



        if batch_index % trainArgs.log_interval == 0:
            sample_idx=batch_index * len(x)
            batch_progress=100. * batch_index / batch_cnt
            print(f'Train Epoch: {epoch} [{sample_idx}/{_TRAIN_SAMPLE_CNT} ({batch_progress:.0f}%)]\tLoss: {loss.item():.6f}  ' )



def test(model:Net,    xTest:torch.Tensor,yTest:torch.Tensor):
    model.eval()#模型设置为测试模式
    test_loss = 0
    correct_avg = 0
    with torch.no_grad():
        x, y = xTest , yTest
        ŷ = model(x)
        test_loss = F.nll_loss(ŷ, y, reduction='sum').item() / _TEST_SAMPLE_CNT  # sum up batch loss
        pred = ŷ.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
        correct_avg = pred.eq(y.view_as(pred)).sum().item()  / _TEST_SAMPLE_CNT


    print(f'\nTest set: Average loss: {test_loss:.6f}, Accuracy: {correct_avg}   ({100. * correct_avg :.0f}%)\n')

args = Args()

torch.manual_seed(args.seed)


trainArgs = TrainArgs()
test_kwargs = TestArgs()

(xTrain,yTrain,xTest,yTest)=loadMnist()

xTrain=torch.Tensor(xTrain).type(torch.float32)
yTrain=torch.Tensor(yTrain).type(torch.int64)
xTest=torch.Tensor(xTest).type(torch.float32)
yTest=torch.Tensor(yTest).type(torch.int64)

model = Net()
optimizer = optim.Adadelta(model.parameters(), lr=args.lr)
scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)



for epoch in range(1, args.epochs + 1):
    train(trainArgs, model,  xTrain,yTrain, optimizer, epoch)
    test(model,   xTest, yTest)
    scheduler.step()

if args.save_model:
    torch.save(model.state_dict(), "mnist_cnn.pt")

# %% [code]
