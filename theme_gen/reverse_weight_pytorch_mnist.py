_TRAIN_SAMPLE_CNT,PIC_H,PIC_W=None,None,None
_TEST_SAMPLE_CNT=None
PIC_HW=None

LINEAR_1_OUT_SIZE=64
R_LINEAR_1_OUT_SIZE=LINEAR_1_OUT_SIZE
CLASS_CNT=10
"""
_TRAIN_SAMPLE_CNT : 60000
_TEST_SAMPLE_CNT : 10000
PIC_H : 28, PIC_W : 28
PIC_HW : 28 * 28
"""

## 想 加载mnist数据集 并自制batch, 而不是用框架自带的制作batch办法
##{用keras 的 mnist 数据集, 因为 keras直接返回的是 numpy.ndarray
##不用pytorch 的 mnist 数据集, 因为 pytorch 返回的mnist dataset :
# 不知道怎么整个转成 numpy.ndarray
# 或  不知道怎么整个转成 torch 完整数组
# 然后用 完整的数组 自制 batch, 这样灵活


import numpy
import numpy as np
def loadMnist() -> (numpy.ndarray,numpy.ndarray,numpy.ndarray,numpy.ndarray):
    """

    :return:  (xTrain,yTrain,xTest,yTest)
    """
    global _TRAIN_SAMPLE_CNT
    global PIC_H
    global PIC_W
    global _TEST_SAMPLE_CNT
    global PIC_HW

    import keras
    import tensorflow
    print(keras.__version__)
    print(tensorflow.__version__)

    # avatar_img_path = "/kaggle/working/data"


    import os

    import cv2

    # img_dataset=load_data()
    # img_dataset.shape
    xTrain:numpy.ndarray; label_train:numpy.ndarray; xTest:numpy.ndarray; label_test:numpy.ndarray
    yTrain:numpy.ndarray; yTest:numpy.ndarray

    #%userprofile%\.keras\datasets\mnist.npz
    (xTrain, label_train), (xTest, label_test) = keras.datasets.mnist.load_data()

    # x_train.shape,y_train.shape, x_test.shape, label_test.shape
    # (60000, 28, 28), (60000,), (10000, 28, 28), (10000,)
    _TRAIN_SAMPLE_CNT,PIC_H,PIC_W=xTrain.shape
    PIC_HW=PIC_H*PIC_W
    xTrain=xTrain.reshape((-1, PIC_H * PIC_W))
    xTest=xTest.reshape((-1, PIC_H * PIC_W))

    _TEST_SAMPLE_CNT=label_test.shape[0]


    from sklearn import preprocessing


    #pytorch 的 y 不需要 oneHot
    #_label_train是1列多行的样子.  _label_train.shape : (60000, 1)
    yTrain=label_train
    # y_train.shape:(60000) ; y_train.dtype: dtype('int')

    CLASS_CNT=yTrain.shape[0]

    yTest=label_test
    # y_test.shape:(10000) ; y_test.dtype: dtype('int')

    xTrainMinMaxScaler:preprocessing.MinMaxScaler; xTestMinMaxScaler:preprocessing.MinMaxScaler
    xTrainMinMaxScaler=preprocessing.MinMaxScaler()
    xTestMinMaxScaler=preprocessing.MinMaxScaler()

    # x_train.dtype: dtype('uint8') -> dtype('float64')
    xTrain=xTrainMinMaxScaler.fit_transform(xTrain)

    # x_test.dtype: dtype('uint8') -> dtype('float64')
    xTest = xTestMinMaxScaler.fit_transform(xTest)

    return (xTrain,yTrain,xTest,yTest)
##}

# copy from https://raw.githubusercontent.com/pytorch/examples/master/mnist/main.py
# from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR


class Args:
    def __init__(self):
        self.epochs = 4
        self.lr = 1.0
        self.gamma = 0.7
        self.no_cuda = False
        self.seed = 1
        self.log_interval = 100
        self.save_model = True

    def update(self, d):
        self.__dict__.update(d.__dict__)


class TrainArgs(Args):
    def __init__(self):
        Args.__init__(self)
        self.batch_size = 50


class TestArgs(Args):
    def __init__(self):
        Args.__init__(self)
        self.batch_size = 1000


class CudaArgs():
    def __init__(self):
        # cuda_kwargs:
        self.num_workers = 1
        self.pin_memory = True
        self.shuffle = True

    def update(self, d):
        self.__dict__.update(d.__dict__)


# Training settings
args = Args()
use_cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)

device = torch.device("cuda" if use_cuda else "cpu")

trainArgs = TrainArgs()
test_kwargs = TestArgs()
if use_cuda:
    cuda_kwargs = CudaArgs()
    trainArgs.update(cuda_kwargs)
    test_kwargs.update(cuda_kwargs)



(xTrain,yTrain,xTest,yTest)=loadMnist()

xTrain=torch.Tensor(xTrain).type(torch.float32)
yTrain=torch.Tensor(yTrain).type(torch.int64)
xTest=torch.Tensor(xTest).type(torch.float32)
yTest=torch.Tensor(yTest).type(torch.int64)

# _TRAIN_SAMPLE_CNT, PIC_HW= xTrain.shape
# _TEST_SAMPLE_CNT = yTest.shape[0]

pass


# print(dataset_train.train_data.shape,dataset_test.test_data.shape)
# train_loader = torch.utils.data.DataLoader(dataset_train, **trainArgs.__dict__)
# test_loader = torch.utils.data.DataLoader(dataset_test, **test_kwargs.__dict__)



class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.linear1 = nn.Linear(PIC_H * PIC_W, LINEAR_1_OUT_SIZE)
        self.linear2 = nn.Linear(LINEAR_1_OUT_SIZE, CLASS_CNT)

    def forward(self, xflat):
        # print(f"xflat:{xflat.dtype}, {xflat.shape}") #torch.float32, torch.Size([50, 784])
        linear1_out = self.linear1(xflat)
        linear1_out_active = F.relu(linear1_out)
        linear2_out = self.linear2(linear1_out_active)
        linear2_out_active = F.log_softmax(linear2_out, dim=1)
        # print(f"linear2_out_active:{linear2_out_active.shape}")
        # linear2_out_active:torch.Size([50, 10])
        return linear2_out_active,linear1_out_active #重点:返回每一层的激活值



class RNet(nn.Module):
    def __init__(self):
        super(RNet, self).__init__()
        self.rlinear1 = nn.Linear(CLASS_CNT, R_LINEAR_1_OUT_SIZE)
        self.rlinear2 = nn.Linear(R_LINEAR_1_OUT_SIZE, PIC_H * PIC_W)

    def forward(self, linear2_out_active):
        # print(f"xflat:{xflat.dtype}, {xflat.shape}") #torch.float32, torch.Size([50, 784])
        rlinear1_out = self.rlinear1(linear2_out_active)
        rlinear1_out_active = F.relu(rlinear1_out)
        rlinear2_out = self.rlinear2(rlinear1_out_active)
        return rlinear2_out




def train(trainArgs:TrainArgs, model:Net, rmodel:RNet, device, xTrain:torch.Tensor,yTrain:torch.Tensor, optimizer,roptimizer, epoch):
    model.train()#模型设置为训练模式
    rmodel.train()#模型设置为训练模式
    batch_size=trainArgs.batch_size
    batch_cnt = int(_TRAIN_SAMPLE_CNT / batch_size)

    for batch_index in range(batch_cnt):
        beginIndex=batch_index * batch_size
        endIndex=(batch_index + 1) * batch_size
        x=xTrain[beginIndex: endIndex]
        y=yTrain[beginIndex:endIndex]
        x, y = x.to(device), y.to(device)
#         print(f"zzz:{data.shape},{target.shape}")

        optimizer.zero_grad() # 用 optimizer.zero_grad() 还是 model.zero_grad(), 试了一下 感觉一样的. 但不知道两者差异
        # model.zero_grad()
        linear2_out_active,linear1_out_active = model(x)#linear2_out_active,linear1_out_active #重点:必须拿到每一层的激活值
        # linear2_out_active=output: ŷ
        # print(f"output:{output.shape} ,{output.dtype} ; y:{y.shape},{y.dtype}")
        # output:torch.Size([50, 10]) ,torch.float32 ; y:torch.Size([50]),torch.int64
        loss = F.nll_loss(linear2_out_active, y)
        loss.backward()
        optimizer.step()

        roptimizer.zero_grad() # 用 roptimizer.zero_grad() 还是 rmodel.zero_grad(), 试了一下 感觉一样的. 但不知道两者差异
        # rmodel.zero_grad()
        linear2_out_active_detach=torch.Tensor(linear2_out_active.detach())#重点:detach
        rlinear2_out  = rmodel(linear2_out_active_detach)
        # print(f"output:{output.shape} ,{output.dtype} ; y:{y.shape},{y.dtype}")
        # output:torch.Size([50, 10]) ,torch.float32 ; y:torch.Size([50]),torch.int64
        x_detach=torch.Tensor(x.detach()) #重点:detach
        rloss = F.mse_loss(rlinear2_out, x_detach) #重点:mse_loss
        # print(f"rloss:{type(rloss.item())}")#rloss:<class 'float'>
        rloss.backward()
        roptimizer.step()

        if batch_index % trainArgs.log_interval == 0:
            sample_idx=batch_index * len(x)
            batch_progress=100. * batch_index / batch_cnt
            print(f'Train Epoch: {epoch} [{sample_idx}/{_TRAIN_SAMPLE_CNT} ({batch_progress:.0f}%)]\tLoss: {loss.item():.6f} \trLoss: {rloss.item():.6f}' )




def test(model:Net,rmodel:RNet, device, xTest:torch.Tensor,yTest:torch.Tensor):
    model.eval()#模型设置为测试模式
    rmodel.eval()#模型设置为测试模式
    test_loss = 0
    correct_avg = 0
    with torch.no_grad():
        x, y = xTest.to(device), yTest.to(device)
        linear2_out_active,linear1_out_active = model(x)#linear2_out_active,linear1_out_active
        #linear2_out_active==out:ŷ
        test_loss = F.nll_loss(linear2_out_active, y, reduction='sum').item() / _TEST_SAMPLE_CNT  # sum up batch loss
        pred = linear2_out_active.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
        correct_avg = pred.eq(y.view_as(pred)).sum().item()  / _TEST_SAMPLE_CNT

        rlinear2_out  = rmodel(linear2_out_active)
        rloss = F.mse_loss(rlinear2_out, x) #重点:mse_loss
        # print(f"rloss:{rloss.shape}") #rloss:torch.Size([])

    print(f'\nTest set: Average loss: {test_loss:.6f}, Accuracy: {correct_avg}, rloss:{rloss:.6f}  ({100. * correct_avg :.0f}%)\n')



model = Net().to(device)
optimizer = optim.Adadelta(model.parameters(), lr=args.lr)
scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)


rmodel = RNet().to(device)
roptimizer = optim.Adadelta(rmodel.parameters(), lr=args.lr)
rscheduler = StepLR(roptimizer, step_size=1, gamma=args.gamma)


for epoch in range(1, args.epochs + 1):
    train(trainArgs, model,rmodel, device, xTrain,yTrain, optimizer,roptimizer, epoch)
    test(model,rmodel, device, xTest, yTest)
    scheduler.step()
    rscheduler.step()

if args.save_model:
    torch.save(model.state_dict(), "mnist_cnn.pt")



