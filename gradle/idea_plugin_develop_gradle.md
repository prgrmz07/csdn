> win10 下 打开 git bash

```bash

export JAVA_HOME=/c/app/jdk-11.0.9
export PATH=$JAVA_HOME/bin:$PATH
export PATH=/d/GRADLE_USER_HOME/wrapper/dists/gradle-6.7-bin/efvqh8uyq79v2n7rcncuhu9sv/gradle-6.7/bin:$PATH

#用clash for window科学上网
export  GRADLE_OPTS="-DsocksProxyHost=127.0.0.1 -DsocksProxyPort=7890    -Dhttps.proxyHost=127.0.0.1  -Dhttps.proxyPort=7890   -Dhttp.proxyHost=127.0.0.1  -Dhttp.proxyPort=7890 -Dcom.sun.net.ssl.checkRevocation=false"



cd /e/idea_plugin_home/
gradle   buildPlugin
```

```bash
#调试方法
#找到文件  将最后一行 
exec "$JAVACMD" "$@"
#之前插入一行
echo "$JAVACMD" "$@"

#之后执行 gradle 即可打印出所执行的命令, 然后直接用改命令、并修改该命令 直到正确为止
```
