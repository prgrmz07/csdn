[目标html页面](https://cloud.tencent.com/developer/article/1751939)


> 以下是neo4j插入语句
```sql


CREATE (节01:SegNode {title:"01 新建一个基于 Gradle 的插件项目",xpath: '/html/body/div[2]/div[1]/div[5]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/h2[1]/strong'}),
  (段01:TextNode {title:"这里我们基于 Gradle 进行插件开发，这也是 IntelliJ 官方的推荐的插件开发解决方案。",xpath: '/html/body/div[2]/div[1]/div[5]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/p[6]'}),
  (段02:TextNode {title:"第一步，选择 Gradle 项目类型并勾选上相应的依赖。",xpath: '/html/body/div[2]/div[1]/div[5]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/p[7]/strong'}),
  (段03:ImageNode {url:"https://ask.qcloudimg.com/http-save/yehe-7276705/2odo39mhm7.png?imageView2/2/w/1620",xpath: '/html/body/div[2]/div[1]/div[5]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/figure[2]/div/span/img'}),
  (段01)-[:父 ]->(节01),
  (段02)-[:父 {fieldA:"valueA"}]->(节01),
  (段03)-[:父 ]->(节01),
  
  (段02)-[:前兄 {childIndex:1}]->(段01),
  (段03)-[:前兄 {childIndex:2}]->(段02)
RETURN 节01, 段01, 段02, 段03
```

![neo4j_create_example.png](https://gitlab.com/prgrmz07/csdn/-/raw/main/neo4j/pic/neo4j_create_example.png)


