[neo4j install](https://blog.csdn.net/weixin_40678266/article/details/103139456)

> 密码置为默认密码:  默认用户neo4j, 默认密码neo4j
>> 解压 neo4j-community-3.5.5.tar  后 删除目录  neo4j-community-3.5.5\data\dbms\

> 仅仅监听localhost
>> neo4j-community-3.5.5\conf\neo4j.conf 第54行注掉
```python
#第54行
#dbms.connectors.default_listen_address=0.0.0.0

```

按 [neo4j_env.bat](https://gitlab.com/prgrmz07/csdn/-/blob/main/neo4j/neo4j_env.bat) 
