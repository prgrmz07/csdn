###  1. hyperV安装centos7X64
###  2.centos7X64常见设置
#### 2.1. yum镜像修改
> [mirror 163 yum镜像修改 参考](https://mirrors.163.com/.help/centos.html)
```bash
cd /etc/yum.repos.d/
mv CentOS-Base.repo CentOS-Base.repo0000
curl -o CentOS-Base.repo https://mirrors.163.com/.help/CentOS7-Base-163.repo

yum clean all && yum makecache
```


####  2.2  关闭 selinux 和防火墙
```bash
systemctl stop firewalld.service && systemctl disable  firewalld.service && iptables -F && setenforce 0

```

####  2.3 修改主机名
```bash
hostnamectl set-hostname rancher_server
```

### 3 rancher

#### 3.1 rancher架构图

![rancher架构图](https://gitlab.com/prgrmz07/csdn/-/raw/main/rancher/drawio/rancher_archv.drawio.png)



> 图中 rancher_server、k8s_master、k8s_node_1 均参照 "2.centos7X64常见设置" 搭建



####  3.2 宿主机win10添加域名解析

```python
#file : C:\Windows\System32\drivers\etc\hosts

172.25.180.62   rancher_server
172.25.180.63   k8s_master
172.25.180.64   k8s_node_1
```

### 3.3 每台服务器都安装docker

```bash
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#安装docker-ce
yum install docker-ce docker-ce-cli containerd.io -y
systemctl start docker && systemctl enable docker.service && systemctl status docker
tee /etc/docker/daemon.json << 'EOF'
{
"registry-mirrors": ["https://换成你自己阿里云账号的串.mirror.aliyuncs.com"]
}
EOF
#阿里云私有镜像地址 需要用自己阿里云账号登陆后申请

#重启docker
systemctl daemon-reload && systemctl restart docker
```



### 3.4  rancher_server安装

```bash
docker run --privileged   -p 80:80 -p 443:443     -e CATTLE_AGENT_IMAGE="registry.cn-hangzhou.aliyuncs.com/rancher/rancher-agent:v2.4.2"    -e HTTP_PROXY=http://172.25.176.1:7890  -e HTTPS_PROXY=http://172.25.176.1:7890   registry.cn-hangzhou.aliyuncs.com/rancher/rancher:v2.4.2

#此时通过 rancher_server 在宿主机win10上的ip https://172.25.181.199 已经能正常访问了
#注意 暂不支持 https://rancher_server  


#172.25.176.1 是hyperv网关 在宿主机win10 上的网卡的ip

```





```bash
#有可能还要在: rancher_server 中 设置代理:
# /root/.docker/config.json
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://172.25.176.1:7890",
     "httpsProxy": "http://172.25.176.1:7890",
     "noProxy": "localhost,127.0.0.1,.example.com"
   }
 }
}


#/etc/profile 中加入:
PROXY_URL="http://172.25.176.1:7890"

export http_proxy="$PROXY_URL"
export https_proxy="$PROXY_URL"
export ftp_proxy="$PROXY_URL"
export no_proxy="127.0.0.1,localhost"

```





> 备注
>
> ```bash
> # "3.4  rancher_server安装" 刚执行完dokcer run后
> 
> #此时还是会报git clone失败, 
> #git clone https://git.rancher.io/system-charts/
> #原因是因为 该dockerfile中执行的git命令没有使用上面设定的proxy 而是用了一个不对的proxy, 但是这里修改不了dockerfile的
> #如果能改dockerfile, 可以加入命令:
> #git config --global --unset http.proxy
> #git config --global --unset https.proxy
> #即可强迫git使用上面设置的正确的proxy了
> ```
>
> 



### 3.5 通过rancher web 修改rancher reopo为国内的

>  https://172.25.181.199

>  rancher web界面修改 system-default-registry配置项
> ![rancher web界面修改 system-default-registry配置项](https://gitlab.com/prgrmz07/csdn/-/raw/main/rancher/pic/rancher%20web%20system-default-registry.png)

>  rancher web 界面修改catalog仓库地址
>
>  ![rancher web 界面修改catalog仓库地址](https://gitlab.com/prgrmz07/csdn/-/raw/main/rancher/pic/rancher%20web%20Catalog.png)







### 3.5 登陆rancher web创建k8s集群

